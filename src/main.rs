mod application;
mod config;
mod window;

use self::application::SubscriptionsApplication;
use self::window::SubscriptionsWindow;

use config::{LOCALEDIR, NAME, PKGDATADIR, PKGNAME};
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::{gio, glib, prelude::*};

fn main() {
    // Set up gettext translations
    bindtextdomain(PKGNAME, LOCALEDIR).expect("Unable to bind the text domain");
    bind_textdomain_codeset(PKGNAME, "UTF-8").expect("Unable to set the text domain encoding");
    textdomain(PKGNAME).expect("Unable to switch to the text domain");

    // Load resources
    let resources = gio::Resource::load(PKGDATADIR.to_owned() + "/subscriptions.gresource")
        .expect("Could not load resources");
    gio::resources_register(&resources);

    glib::set_application_name(NAME);

    // Create a new GtkApplication. The application manages our main loop,
    // application windows, integration with the window manager/compositor, and
    // desktop features such as file opening and single-instance applications.
    let app =
        SubscriptionsApplication::new("org.yxnan.Subscriptions", &gio::ApplicationFlags::empty());

    // Run the application. This function will block until the application
    // exits. Upon return, we have our exit code to return to the shell. (This
    // is the code you see when you do `echo $?` after running a command in a
    // terminal.
    std::process::exit(app.run());
}
