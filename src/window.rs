use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/yxnan/Subscriptions/gtk/window.ui")]
    pub struct SubscriptionsWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SubscriptionsWindow {
        const NAME: &'static str = "SubscriptionsWindow";
        type Type = super::SubscriptionsWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SubscriptionsWindow {}
    impl WidgetImpl for SubscriptionsWindow {}
    impl WindowImpl for SubscriptionsWindow {}
    impl ApplicationWindowImpl for SubscriptionsWindow {}
}

glib::wrapper! {
    pub struct SubscriptionsWindow(ObjectSubclass<imp::SubscriptionsWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl SubscriptionsWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::new(&[("application", application)])
            .expect("Failed to create SubscriptionsWindow")
    }
}
